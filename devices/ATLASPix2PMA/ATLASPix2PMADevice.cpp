/** Caribou bare device example implementation
 */

#include "ATLASPix2PMADevice.hpp"

#include "hal.hpp"
#include "log.hpp"

#include <chrono>
#include <cmath>
#include <ctime>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>
#include <bitset>
#include <sys/time.h>



using namespace caribou;
ATLASPix2PMADevice::ATLASPix2PMADevice(const caribou::Configuration config)
: pearyDevice(config, std::string(DEFAULT_DEVICEPATH), ATLASPix2PMA_DEFAULT_I2C) {
 // _dispatcher.add("frobicate", &ATLASPix2PMADevice::frobicate              , this);
  //_dispatcher.add("unfrobicate", &ATLASPix2PMADevice::unfrobicate, this);
  _dispatcher.add("writeSync", &ATLASPix2PMADevice::writeSync, this);
  _dispatcher.add("Write", &ATLASPix2PMADevice::Write, this);
  _dispatcher.add("Read", &ATLASPix2PMADevice::Read, this);
  _dispatcher.add("Sync", &ATLASPix2PMADevice::Sync_command, this);
  _dispatcher.add("Write_normal", &ATLASPix2PMADevice::Write_command, this);
  _dispatcher.add("Read_normal", &ATLASPix2PMADevice::Read_command, this);
  _dispatcher.add("Read_registers", &ATLASPix2PMADevice::Read_input_output_registers, this);
  _dispatcher.add("Read_bypass", &ATLASPix2PMADevice::Bypass_read_command, this);
  _dispatcher.add("Write_bypass", &ATLASPix2PMADevice::Bypass_write_command, this);
  _dispatcher.add("ReadTemperature", &ATLASPix2PMADevice::ReadTemperature, this);
  _dispatcher.add("powerStatusLog", &ATLASPix2PMADevice::powerStatusLog, this);
  _dispatcher.add("Start_Test", &ATLASPix2PMADevice::Start_Test, this);
  _dispatcher.add("setPMAReset", &ATLASPix2PMADevice::SetPMAReset, this);



  _registers.add(ATLASPix2PMA_REGISTERS);



  //this->writeSync(); //because at the beginning output register is 0xFFFF and so the polling technique fails
  pearyDevice<iface_i2c>::configure();

  _periphery.add("VDDD", PWR_OUT_6);
  _periphery.add("VCC25", PWR_OUT_5);
  _periphery.add("Reset", BIAS_12);
  _periphery.add("MatrixVDDD", PWR_OUT_4);

}

ATLASPix2PMADevice::~ATLASPix2PMADevice() {
  LOG(INFO) << DEVICE_NAME << ": shutdown, delete device";
}

std::string ATLASPix2PMADevice::getFirmwareVersion() {
  return "42.23alpha";
}

double ATLASPix2PMADevice::ReadTemperature(){


	//Read data from DS25095A
	std::vector<uint8_t> data = _hal->readLongRegister(DEFAULT_DEVICEPATH,ATLASPix2PMA_DEFAULT_I2C,0b00000101,2);
    uint8_t UpperByte = data.at(0);
    uint8_t LowerByte = data.at(1);

    //Convert the temperature data
    double Temperature = 0;
    UpperByte = UpperByte & 0x1F;       //Clear flag bits

    if ((UpperByte & 0x10) == 0x10){
    	UpperByte = UpperByte & 0x0F;   //Clear SIGN
    	Temperature = 256 - (double(UpperByte) * 16 + double(LowerByte) / 16);
    }

    else {
    	Temperature = (double(UpperByte) * 16 + double(LowerByte) / 16);
	}

    LOG(INFO) << "ATLASPix2 PCB Temperature : "<< Temperature << " ºC" << std::endl;

    return Temperature;

}

void ATLASPix2PMADevice::configure(){

    LOG(INFO) << "Setting up clocking for PMA";
	this->configureClock();
}


void ATLASPix2PMADevice::SetPMAReset(bool value){

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* resetPMA = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x8);
	  	  *resetPMA = value & 0b1;

}


void ATLASPix2PMADevice::reset(bool state){

	if(state==true)this->setVoltage("Reset",0,1);
	//sleep(1);
	else this->setVoltage("Reset",1.8,1);
}

void ATLASPix2PMADevice::configureClock() {
  /*
    // Check of we should configure for external or internal clock, default to external:
    if(_config.Get<bool>("clock_internal", false)) {
      LOG(DEBUG) << DEVICE_NAME << ": Configure internal clock source, free running, not locking";
      _hal->configureSI5345((SI5345_REG_T const* const)si5345_revb_registers_free, SI5345_REVB_REG_CONFIG_NUM_REGS_FREE);
      mDelay(100); // let the PLL lock
    } else {
  */
  LOG(DEBUG) << DEVICE_NAME << ": Configure external clock source, locked to TLU input clock";
  _hal->configureSI5345((SI5345_REG_T const* const)si5345_revb_registers, SI5345_REVB_REG_CONFIG_NUM_REGS);
  LOG(DEBUG) << "Waiting for clock to lock...";

  // Try for a limited time to lock, otherwise abort:
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
  while(!_hal->isLockedSI5345()) {
    auto dur = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start);
    if(dur.count() > 0.5)
      // throw DeviceException("Cannot lock to external clock.");
      break;
  }
  if(_hal->isLockedSI5345())
    LOG(INFO) << "PLL locked to external clock...";
  else
    LOG(INFO) << "Cannot lock to external clock, PLL will continue in freerunning mode...";
  //  }
  LOG(INFO) << "Done";

}



std::string ATLASPix2PMADevice::getName() {
  return DEVICE_NAME;
}

void ATLASPix2PMADevice::daqStart() {
  LOG(INFO) << DEVICE_NAME << ": daq started";
}

void ATLASPix2PMADevice::daqStop() {
  LOG(INFO) << DEVICE_NAME << ": daq stopped";
}

std::vector<uint32_t> ATLASPix2PMADevice::getRawData() {
  return {0u, 2u, 4u, 1u, 3u, 5u};
}

pearydata ATLASPix2PMADevice::getData() {
  pearydata x;
  x[{0u, 0u}] = std::make_unique<pixel>();
  x[{8u, 16u}] = std::make_unique<pixel>();
  return x;
}

std::vector<std::pair<std::string, uint32_t>> ATLASPix2PMADevice::getRegisters() {
  return {{"reg0", 0u}, {"reg1", 1u}};
}

std::vector<uint64_t> ATLASPix2PMADevice::timestampsPatternGenerator() {
  return {0u, 1u};
}

// custom device functionality exported via the dispatcher

int32_t ATLASPix2PMADevice::frobicate(int32_t a) {
  return (a << 2) & 0b010101010101010101010101010101;
}

std::string ATLASPix2PMADevice::unfrobicate(int32_t b) {
  return "blub" + std::to_string(b);
}

//mie funzioni prova
void ATLASPix2PMADevice::writeSync() {

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* W_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* R_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);


	uint32_t temp;

	LOG(INFO) << DEVICE_NAME << ": Read register :"  << *R_address;

	LOG(INFO) << DEVICE_NAME << ": write_register :" << *W_address;

	int k=0;
	*W_address=0x40000000 ;//Sync
	LOG(INFO) << DEVICE_NAME << "Sending Sync command";



	temp=*R_address &0b1; //&0b1
	LOG(INFO) << DEVICE_NAME << ": temp :"<<temp;

	while(temp>0)
	{
		temp= *R_address &0b1;
		k++;
		if(k<10)
				LOG(INFO) << DEVICE_NAME << ": temp :"<<temp;

		if(k>1000){
			LOG(INFO) << DEVICE_NAME << "Timeout";
			break;

		}
	}

}



void ATLASPix2PMADevice::Send_command(uint32_t val) {

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* W_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* R_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);

	  uint32_t temp;

	  temp=*R_address &0b1; //&0b1

	  while(temp)
	  	  {
		  temp= *R_address &0b1 ;
	  	  }

	  *W_address=val  ;//Scrittura
	  LOG(INFO) << DEVICE_NAME << ": command sent";
}





void ATLASPix2PMADevice::Write_command(uint16_t val, uint16_t address) {

	uint32_t command= (   (this->Write_const<<30) | (this->ID_const<<25) |  ((uint32_t)(address & 0b111111111) )<<16 | val );
	// if also the address could be generated randomly, then an "and" operation is needed
	LOG(INFO) << DEVICE_NAME << ": write command: "<<std::hex<<command;


	this->Send_command(command);
}

void ATLASPix2PMADevice::Sync_command() {

//	uint32_t command= this->Sync_const<<30;
//	LOG(INFO) << DEVICE_NAME << ": sync command: "<<std::hex<<command;
//
//
//	this->Send_command(command);

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* command = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* start = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);
	  	  volatile uint32_t* repeat = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x10);

	  	  *command = 0x0001;
	  	  *repeat = 10;
	  	  *start = 1;
	  	   usleep(100);
	  	  *start = 0;

}


void ATLASPix2PMADevice::Write(uint16_t val, uint16_t address) {

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* command = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* start = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);
	  	  volatile uint32_t* Address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x8);
	  	  volatile uint32_t* data = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0xC);
	  	  volatile uint32_t* repeat = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x10);

	  	  *command = 0x0003;
	  	  *Address = address & 0b111111111;
	  	  *data = val & 0b1111111111;
	  	  *repeat = 1;

	  	  *start = 1;
	  	  usleep(100);
	  	  *start = 0;


}

void ATLASPix2PMADevice::Read( uint16_t address) {

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* command = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* start = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);
	  	  volatile uint32_t* Address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x8);
	  	  volatile uint32_t* repeat = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x10);

	  	  *command = 0x0002;
	  	  *Address = address & 0b111111111;
	  	  *repeat = 1;
	  	  *start = 1;
	  	  usleep(100);
	  	  *start = 0;


}

uint32_t ATLASPix2PMADevice::Read_command(uint16_t address) {

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* W_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* R_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);
	  	  uint32_t temp;
	  	  uint32_t read_value=0;

	uint32_t command= (   (this->Read_const<<30) | (this->ID_const<<25) |  ((uint32_t)(address & 0b111111111) )<<16  ); 	// if also the address could be generated randomly, then an "and" operation is needed


	LOG(INFO) << DEVICE_NAME << ": read command: "<<std::bitset<32>(command);



	int k=0;
//	temp=*R_address &0b1; //&0b1
//	while(temp>0)
//	{
//	  if (k<100) // funzione per sbloccare da attesa continua da memopria
//	  {
//		  temp= *R_address &0b1 ;
//		  k++;
//	  }
//	  else
//		  this->writeSync();
//	}


	*W_address= command ;//Scrittura
	LOG(INFO) << DEVICE_NAME << ": command sent";


	temp=*R_address &0b1; //&0b1
	k=0;
	while(temp>0)
	{
	temp= *R_address &0b1 ;
	k++;

	if(k>1000){
		LOG(INFO) << "timeout  " << std::bitset<32>(*R_address);
		break;
		}
	}

	read_value=*R_address;
	LOG(INFO) << DEVICE_NAME << ":Read command finished. Read value: "<< read_value;


	return *R_address;

}


void ATLASPix2PMADevice::Bypass_write_command(uint16_t val, uint16_t address) {

	uint32_t command= (   (this->Bypass_write_const<<28) |  ((uint32_t)(address & 0b111))<<25 | ((uint32_t) (val & 0b1111) )<<21 );
	LOG(INFO) << DEVICE_NAME << ": bypass write command: "<<std::hex<<command;


	this->Send_command(command);
}


uint32_t ATLASPix2PMADevice::Bypass_read_command(uint16_t address) {

	void* WR_base =
	  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
	  	  volatile uint32_t* W_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
	  	  volatile uint32_t* R_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);
	  	  uint32_t temp;
	  	  uint32_t read_value=0;

	  	uint32_t command= (   (this->Bypass_read_const<<28) |  ((uint32_t)address)<<25);
		LOG(INFO) << DEVICE_NAME << ": bypass read command: "<<std::hex<<command;



	  temp=*R_address &0b1; //&0b1
	  while(temp>0)
	  {temp= *R_address &0b1 ;}


	*W_address= command  ;//Scrittura
	LOG(INFO) << DEVICE_NAME << ": command sent";


		  temp=*R_address &0b1; //&0b1
		  while(temp>0)
		  {temp= *R_address &0b1 ;}

		  read_value=*R_address;
		  LOG(INFO) << DEVICE_NAME << ":Bypass read command finished. Read value: "<< read_value;


		  return *R_address;

}


void ATLASPix2PMADevice::Initialize_0(int bypass_mode){



	if(bypass_mode!=1) // normal test
		for (int j=0; j<340; j++)
			this->Write_command(this->Initialize_0_const, (uint16_t)j);
	else // bypass test
		for (int j=0; j<8; j++)
			this->Bypass_write_command(this->Initialize_0_const, (uint16_t)j);


}

void ATLASPix2PMADevice::Initialize_1(int bypass_mode){


	if(bypass_mode!=1) // normal test
		for (int j=0; j<340; j++)
			this->Write_command(this->Initialize_1_const, (uint16_t)j);
	else // bypass test
		for (int j=0; j<8; j++)
			this->Bypass_write_command(this->Initialize_1_const, (uint16_t)j);
}

void ATLASPix2PMADevice::Initialize_chessboard1(int bypass_mode){

	if(bypass_mode!=1) // normal test
		for (int j=0; j<340; j++)
			this->Write_command(this->Initialize_chessboard1_const, (uint16_t)j);
	else // bypass test
		for (int j=0; j<8; j++)
			this->Bypass_write_command(this->Initialize_chessboard1_const, (uint16_t)j);
}

void ATLASPix2PMADevice::Initialize_chessboard2(int bypass_mode){

	if(bypass_mode!=1) // normal test
		for (int j=0; j<340; j++)
			this->Write_command(this->Initialize_chessboard2_const, (uint16_t)j);
	else // bypass test
		for (int j=0; j<8; j++)
			this->Bypass_write_command(this->Initialize_chessboard2_const, (uint16_t)j);

}

void ATLASPix2PMADevice::Initialize_rand(int bypass_mode){

	uint16_t val_random;
	uint16_t val_bypass_written=0;
	uint16_t temp=0;
	if(bypass_mode!=1)
		for (int j=0; j<340; j++)
			{
				val_random=rand();
				this->Write_command((uint16_t)val_random, (uint16_t)j);
				//this->random_value.insert(std::make_pair((uint16_t)j,(uint16_t)val_random));
				this->random_value[(uint16_t)j]=(uint16_t)val_random;

			}
	else
		for (int j=0; j<8; j++)
			{
				val_random=rand()%16;
				temp= val_random & 0b1111 ;
				this->Bypass_write_command((uint16_t)val_random, (uint16_t)j);
				val_bypass_written=(  temp<<12 | temp<<8 | temp<<4 | temp );
				//this->random_value.insert(std::make_pair((uint16_t)j,(uint16_t)val_random));
				this->random_value[(uint16_t)j]=val_bypass_written;

			}



}



void ATLASPix2PMADevice::Initialize_Matrix(int test_type, std::fstream* logFile, int bypass_mode){
	if(test_type==0)
		this->Initialize_0(bypass_mode);
	else if(test_type==1)
		this->Initialize_1(bypass_mode);
	else if(test_type==2)
		this->Initialize_chessboard1(bypass_mode);
	else if(test_type==3)
		this->Initialize_chessboard2(bypass_mode);
	else if(test_type==4)
		this->Initialize_rand(bypass_mode);

	if(test_type !=4){

		*logFile<<this->GetTimestamp()<<"refresh matrix with constant value of this type of test "<<std::endl;


	}
	else
	{
		*logFile<<this->GetTimestamp()<<"refresh matrix with generated random values: "<<std::endl;

		if(bypass_mode!=1) // normal test
				for (int j=0; j<340; j++)
					*logFile<<"address: "<< j << "    data: "<<  this->random_value[j] << std::endl;
			else // bypass test
				for (int j=0; j<8; j++)
					*logFile<<"address: "<< j << "    data: "<<  this->random_value[j] << std::endl;


	}






}


void ATLASPix2PMADevice::UpdateMatrix ( uint16_t address,  uint16_t xor_result  ){
	std::vector <int> positions;
	short unsigned temp;
	int k=0;

	// create position vector
	for (int g=0; g<16; g++)
	{
		temp=xor_result%2;
		if(temp>0)
			positions.push_back(g);
		xor_result=xor_result>>1;
	}

	// update matrix
	for(k=positions.size(); k>0; k--)
	{
		this->Matrix_SEU[address][positions.back()] ++;
		positions.pop_back();

	}


}

void ATLASPix2PMADevice::Read_matrix (int test_type, std::fstream* logFile, int bypass_mode){

	uint16_t xor_ris;
	uint32_t read_data;
	int i_max=0;
	*logFile<<this->GetTimestamp()<<"Read values: "<<std::endl;

	if(bypass_mode !=1) //normal test
		i_max=340;
	else
		i_max=8;


	for(int i; i<i_max; i++)
	{

		if(bypass_mode !=1) //normal test
			read_data=this->Read_command(i);
		else
			read_data=this->Bypass_read_command(i);



		if(test_type==0)
			  xor_ris= (  (read_data >> 7)  ^ this->Initialize_0_const  );
		else if(test_type==1)
			  xor_ris= (  (read_data >> 7)   ^ this->Initialize_1_const   );
		else if(test_type==2)
			  xor_ris= (  (read_data >> 7)   ^ this->Initialize_chessboard1_const   );
		else if(test_type==3)
			  xor_ris= (  (read_data>> 7)   ^ this->Initialize_chessboard2_const   );
		else
			  xor_ris= (  (read_data >> 7)   ^ this->random_value[(uint16_t)i]   );

		*logFile<<"address: "<< (uint16_t)(read_data>>23)   << "    data: "<<  std::bitset<16> ( (uint16_t)(read_data>>7)  )<< "    errors: "<< std::bitset<16> ((uint16_t)xor_ris)<< std::endl;

		this->UpdateMatrix(i,xor_ris);



	}





}


void ATLASPix2PMADevice::Start_Test (int test_type, int bypass_mode, int duration, int interval_read_and_refresh, std::string logPath){

	std::fstream logFile;


	long start_time=0;
	long elapsed=0;
	long current_time=0;
	struct timeval t1;
	struct timezone t2;

	 srand((unsigned)time(NULL));


	logFile.open(logPath, std::ios_base::out |std::ios_base::app);
	if(!logFile.is_open()){

		LOG(INFO) << DEVICE_NAME << "Open logFile error. Exit from Test:";

						  }
	else{
		logFile<<this->GetTimestamp()<<" Test start, type: "<<test_type<<"Bypass: "<<bypass_mode<<std::endl;


				gettimeofday(&t1, &t2);
				start_time=t1.tv_sec;


				elapsed=0;

				while(elapsed<duration){


				this->Initialize_Matrix(test_type, &logFile, bypass_mode);
			    // scrivi nel log che hai refrashato
				logFile<<this->GetTimestamp()<<"refrash matrix: "<<std::endl;

			    sleep(interval_read_and_refresh);
			    this->Read_matrix(test_type, &logFile, bypass_mode);

			    gettimeofday(&t1, &t2);
			    current_time=t1.tv_sec;

			    elapsed=current_time-start_time;


										}



	   }

}

std::string ATLASPix2PMADevice::GetTimestamp(){
	time_t now=time(NULL);
	char buf[256];
	strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", localtime(&now));
	return buf;
}


// for debug
void ATLASPix2PMADevice::Read_input_output_registers(){

	void* WR_base =
		  	  _hal->getMappedMemoryRW(ATLASPix2PMA_WR_BASE_ADDRESS, ATLASPix2PMA_WR_MAP_SIZE, ATLASPix2PMA_WR_MASK);
		  	  volatile uint32_t* W_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x0);
		  	  volatile uint32_t* R_address = reinterpret_cast<volatile uint32_t*>(reinterpret_cast<std::intptr_t>(WR_base) + 0x4);
		  	  uint32_t temp;


		  	  temp=*W_address;
		      std::bitset<32> input_register_bit (temp);
              LOG(INFO) << DEVICE_NAME << ": input_register : "<<std::hex<< temp;


		  	  temp=*R_address;
			  LOG(INFO) << DEVICE_NAME << ": output_register :"<<std::hex<< temp;




}



void ATLASPix2PMADevice::powerUp() {
  LOG(INFO) << DEVICE_NAME << ": Powering up ATLASPix2";
  std::cout << '\n';

  this->setVoltage("VCC25", ATLASPix2PMA_VCC25, ATLASPix2PMA_VCC25_CURRENT);
  this->switchOn("VCC25");

  usleep(200000);

  this->setVoltage("VDDD", ATLASPix2PMA_VDDD, ATLASPix2PMA_VDDD_CURRENT);
  this->switchOn("VDDD");

  this->setVoltage("MatrixVDDD", 1.8, 1);
  this->switchOn("MatrixVDDD");


  this->setVoltage("Reset",1.8,1);
  this->switchOn("Reset");

  usleep(200000);

}


void ATLASPix2PMADevice::powerStatusLog() {
  LOG(INFO) << DEVICE_NAME << " power status:";

  LOG(INFO) << "VDDD:";
  LOG(INFO) << "\tBus voltage: " << _hal->measureVoltage(PWR_OUT_6) << "V";
  LOG(INFO) << "\tBus current: " << _hal->measureCurrent(PWR_OUT_6) << "A";

  LOG(INFO) << "VCC25:";
  LOG(INFO) << "\tBus voltage: " << _hal->measureVoltage(PWR_OUT_5) << "V";
  LOG(INFO) << "\tBus current: " << _hal->measureCurrent(PWR_OUT_5) << "A";

  LOG(INFO) << "MAtrixVDDD:";
  LOG(INFO) << "\tBus voltage: " << _hal->measureVoltage(PWR_OUT_4) << "V";
  LOG(INFO) << "\tBus current: " << _hal->measureCurrent(PWR_OUT_4) << "A";


}



void ATLASPix2PMADevice::powerDown(){
	LOG(INFO) << DEVICE_NAME << ": Powering up ATLASPix2";
	std::cout << '\n';

	this->switchOff("VCC25");

	usleep(200000);

	this->switchOff("VDDD");

	usleep(200000);
}




