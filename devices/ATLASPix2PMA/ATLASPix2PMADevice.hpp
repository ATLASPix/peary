/** Example caribou device
 *
 * Use this class as a starting point for a device that does not rely on
 * an underlying hardware abstraction layer.
 */

#ifndef ATLASPix2PMA_DEVICE_H
#define ATLASPix2PMA_DEVICE_H
//io
#include <algorithm>
#include <atomic>
#include <bitset>
#include <cstdlib>
#include <map>
#include <string>
#include <thread>
#include <map>
#include <fstream>

#include "ATLASPix2PMA_defaults.hpp"

#include "device.hpp"
// io
#include "pearydevice.hpp"
#include "i2c.hpp"



namespace caribou {
  /** Example caribou device class definition
   *
   * This class directly implements all purely virtual functions of
   * `caribou::caribouDevice` without a hardware abstration layer.
   * Most functions are noops. Applications can then control this
   * device via the Caribou device class interface by using the device
   * manager to instanciate the device object.
   */
  class ATLASPix2PMADevice : public pearyDevice<iface_i2c> {
  public:
    ATLASPix2PMADevice(caribou::Configuration config);
    ~ATLASPix2PMADevice();

    std::string getFirmwareVersion();
    std::string getName();

    // Controll the device
    void reset(bool state);
    void configure();
    void configureClock();
    void SetPMAReset(bool value);
    void powerStatusLog();
    void daqStart();
    void daqStop();
    // Retrieve data
    std::vector<uint32_t> getRawData();
    pearydata getData();
    // Configure the device
    void setRegister(std::string, uint32_t){};
    uint32_t getRegister(std::string) { return 0u; };
    std::vector<std::pair<std::string, uint32_t>> getRegisters();
    std::vector<uint64_t> timestampsPatternGenerator();
    // Voltage regulators and current sources
    void setInjectionBias(std::string, double){};
    void setCurrent(std::string, int, bool){};
    double ReadTemperature();
    // Slow ADCs
    double getADC(std::string) { return 0.0; };
    double getADC(uint8_t) { return 0.0; };

    void writeSync();
    void Send_command(uint32_t val);
    void Sync_command();
    void Write_command(uint16_t val, uint16_t address);
    void Write(uint16_t val, uint16_t address);
    void Read(uint16_t address);

    uint32_t Read_command(uint16_t address);
    void Bypass_write_command(uint16_t val, uint16_t address);
    uint32_t Bypass_read_command(uint16_t address);
    void Initialize_0(int bypass_mode);
    void Initialize_1(int bypass_mode);
    void Initialize_chessboard1(int bypass_mode);
    void Initialize_chessboard2(int bypass_mode);
    void Initialize_rand(int bypass_mode);
    void Initialize_Matrix(int test_type, std::fstream* logFile, int bypass_mode );
    void UpdateMatrix (uint16_t address, uint16_t xor_result );
    void Read_matrix (int test_type, std::fstream* logFile, int bypass_mode  );
    void Start_Test (int test_type, int bypass_mode, int duration, int interval_read_and_refresh, std::string logPath); // bypass=1 per bypass test
    std::string GetTimestamp();

    // for debug test
    void Read_input_output_registers();



    /** Turn on the power supply for the ATLASPix chip
         */
    	void powerUp();

        /** Turn off the ATLASPix power
         */
        void powerDown();

  private:
    int32_t frobicate(int32_t a);
    std::string unfrobicate(int32_t b);
    uint32_t const Sync_const= 0b01;
    uint32_t const Write_const= 0b11;
    uint32_t const Read_const= 0b10;
    uint32_t const Bypass_write_const= 0b0011;
    uint32_t const Bypass_read_const= 0b0010;

    uint32_t const ID_const= 0b00000; // 5 bits and not 4 because it is also included the 0 bit following the ID sequence
    uint16_t const Initialize_0_const=0;
    uint16_t const Initialize_1_const=0xFFFF;
    uint16_t const Initialize_chessboard1_const=0xAAAA;
    uint16_t const Initialize_chessboard2_const=0x5555;
    //std::map<uint16_t, uint16_t> random_value;
    std::vector<uint16_t> random_value;

//    unsigned short const rows = 340;
//    unsigned short const column= 16;
    uint16_t Matrix_SEU [340] [16]={0};
    //std::vector <uint16_t> confronti (340);
    //uint16_t confronti [360]={0};

  };

} // namespace caribou

#endif /* ATLASPix2PMA_DEVICE_H */
