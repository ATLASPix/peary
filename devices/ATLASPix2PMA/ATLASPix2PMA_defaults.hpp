#ifndef DEVICE_ATLASPix2PMA_DEFAULTS_H
#define DEVICE_ATLASPix2PMA_DEFAULTS_H

#include "carboard.hpp"
#include "dictionary.hpp"
#include "ATLASPix2PMA_clk_100-FMC_REF.h"

namespace caribou {

// clang-format off
/** Default device path for this device: SEAF-connector I2C bus on BUS_I2C2
 */
#define DEFAULT_DEVICEPATH BUS_I2C2

/** Default I2C address for standalone-ATLASPix board with unconnected I2C address lines
 */
#define ATLASPix2PMA_DEFAULT_I2C 0x50 //ho messo p2. Prima solo ATLASPix

  /** Definition of default values for the different DAC settings for ATLASPix
   */

#define ATLASPix2PMA_VCC25 2.5
#define ATLASPix2PMA_VCC25_CURRENT 2

#define ATLASPix2PMA_VDDD 1.8
#define ATLASPix2PMA_VDDD_CURRENT 2



// ATLASPix2PMA Pixel Memory Control AXI
/** const std::intptr_t ATLASPix2PMA_PixelMemory_BASE_ADDRESS = 0x43C10000;
const std::size_t ATLASPix2PMA_PixelMemory_MAP_SIZE = 4096;
const std::uint32_t ATLASPix2PMA_PixelMemory_MASK = ATLASPix2PMA_PixelMemory_MAP_SIZE - 1;
 */
const std::intptr_t ATLASPix2PMA_WR_BASE_ADDRESS = 0x43C00000;
const std::size_t ATLASPix2PMA_WR_MAP_SIZE = 128;
const std::uint32_t ATLASPix2PMA_WR_MASK = ATLASPix2PMA_WR_MAP_SIZE - 1;


/** Dictionary for register address/name lookup for ATLASPix
 */
#define ATLASPix2PMA_REGISTERS				\
  {						\
    {"unlock", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"BLResPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ThResPix", register_t<>(0x26, 0xFF, false, true, true)},	\
    {"VNPix", register_t<>(0x26, 0xFF, false, true, true)},	\
    {"VNFBPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNFollPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNRegCasc", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VDel", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPComp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDAC", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNPix2", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"BLResDig", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNBiasPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPLoadPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNOutPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPVCO", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNVCO", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDelDclMux", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDelDclMux", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDelDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDelDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDelPreEmp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDelPreEmp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNLVDS", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNLVDSDel", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPPump", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"nu", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"RO_res_n", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Ser_res_n", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Aur_res_n", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"sendcnt", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"resetckdivend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"maxcycend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"slowdownend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"timerend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ckdivend2", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ckdivend", register_t<>(0x26, 0xFF, false, true, true)},              \
    {"VPRegCasc", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPRamp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNcompPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPFoll", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDACPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPBiasRec", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNBiasRec", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Invert", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelEx", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelSlow", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"EnPLL", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"TriggerDelay", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Reset", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ConnRes", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelTest", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelTestOut", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"BLPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"nu2", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ThPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"nu3", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_mode", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ro_enable", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"armduration", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_injection", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"edge_sel", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_enable", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"busy_when_armed", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_always_armed", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"t0_enable", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"gray_decode", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"send_fpga_ts", register_t<>(0x26, 0xFF, false, true, true)},		\
  }
  // clang-format on

  //{"ext_clk", register_t<>(0x26, 0xFF, false, true, true)},		\

} // namespace caribou

#endif /* DEVICE_ATLASPix_DEFAULTS_H */
