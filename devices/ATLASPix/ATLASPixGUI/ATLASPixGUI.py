import sys,time
import matplotlib as plt
import matplotlib.pyplot as pypl
# Make sure that we are using QT5
plt.use('Qt5Agg')
pypl.xkcd()

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import numpy as np


from ATLASPixGUI_MainWindow import Ui_ATLASPixGUI


class MatplotlibMatrixWidget(FigureCanvas):
    def __init__(self, parent=None, title='Title', xlabel='x label', ylabel='y label', dpi=100, hold=False,sizex=290,sizey=580):
        super(MatplotlibMatrixWidget, self).__init__(Figure(tight_layout=True))

        self.resize(sizex,sizey)
        self.setParent(parent)

        self.figure = Figure(dpi=dpi,tight_layout=True,figsize=(0.2,1))
        self.figure.set_facecolor("#F7F6F6")
        self.canvas = FigureCanvas(self.figure)
        self.figure.set_canvas(self.canvas)
        self.theplot = self.figure.add_subplot(111)
        self.theplot.set_title(title)
        self.theplot.set_xlabel(xlabel)
        self.theplot.set_ylabel(ylabel)



        #self.figure.tight_layout()


    def FillTDACMap(self, filename):

        #cmap = matplotlib.get_cmap('PiYG')
        #norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

        x=[]
        y=[]
        w=[]

        for col in range(25):
            for row in range(400):
                x.append(col)
                y.append(row)
                w.append(int(np.random.randn()))




        self.theplot.hist2d(x,y,bins=[25,400],weights=w)
        self.draw()

    def FillMaskMap(self, filename):

        x=[]
        y=[]
        w=[]

        for col in range(25):
            for row in range(400):
                x.append(col)
                y.append(row)
                if int(np.random.randn())>3:
                    w.append(0)
                else:
                    w.append(1)
        self.theplot.hist2d(x,y,bins=[25,400],weights=w)
        self.draw()



class MatplotlibPlotWidget(FigureCanvas):
    def __init__(self, parent=None, title='Title', xlabel='x label', ylabel='y label', dpi=100, hold=False,sizex=780,sizey=330):
        super(MatplotlibPlotWidget, self).__init__(Figure(tight_layout=True))

        self.resize(sizex,sizey)
        self.setParent(parent)

        self.figure = Figure(dpi=dpi,tight_layout=True,figsize=(0.2,1))
        self.figure.set_facecolor("#F7F6F6")
        self.canvas = FigureCanvas(self.figure)
        self.figure.set_canvas(self.canvas)
        self.theplot = self.figure.add_subplot(111)
        self.theplot.set_title(title)
        self.theplot.set_xlabel(xlabel)
        self.theplot.set_ylabel(ylabel)

    def PlotPower(self,interface):

        x=[]
        y=[]

        for i in range(25):
            x.append(i)
            y.append(i*i)
        self.theplot.plot(x,y)
        self.draw()


class MatplotlibHistoWidget(FigureCanvas):
    def __init__(self, parent=None, title='Title', xlabel='x label', ylabel='y label', dpi=100, hold=False,sizex=370,sizey=300):
        super(MatplotlibHistoWidget, self).__init__(Figure(tight_layout=True))

        self.resize(sizex,sizey)
        self.setParent(parent)

        self.figure = Figure(dpi=dpi,tight_layout=True)
        self.figure.set_facecolor("#F7F6F6")
        self.canvas = FigureCanvas(self.figure)
        self.figure.set_canvas(self.canvas)
        self.theplot = self.figure.add_subplot(111)
        self.theplot.set_title(title)
        self.theplot.set_xlabel(xlabel)
        self.theplot.set_ylabel(ylabel)

    def PlotHisto(self,interface):

        x=[]

        for i in range(250):
            x.append(int(np.random.randn()))
        self.theplot.hist(x)
        self.draw()



class MonitorPowerThread(QThread):

    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()
    def run(self):
        while(1):
            print time.time()
            time.sleep(1)



class ATLASPixGUI(QMainWindow):

    isMonitoringPower = False
    PowerMonitorWorker=0

    def MonitorPowerClicked(self):
        if self.isMonitoringPower:
            self.isMonitoringPower=False
            self.PowerMonitorWorker.terminate()
        else:
            self.isMonitoringPower=True
            self.PowerMonitorWorker=MonitorPowerThread()
            self.PowerMonitorWorker.start()




    def MonitorPowerThread(self):
        return int(np.random.randn())

    def __init__(self):
        super(ATLASPixGUI, self).__init__()
        self.ui=Ui_ATLASPixGUI()
        self.ui.setupUi(self)



        self.TDACplot =  MatplotlibMatrixWidget(self.ui.TDACMap,title="TDAC Map",xlabel="Columns",ylabel="rows")
        self.Maskplot =  MatplotlibMatrixWidget(self.ui.MaskMap,title="Mask Map",xlabel="Columns",ylabel="rows")
        self.powerPlot =  MatplotlibPlotWidget(self.ui.powerplot,title="Power consumption",xlabel="time",ylabel="current")
        self.TS1Plot =  MatplotlibHistoWidget(self.ui.TS1plot,title="Timestamp distribution",xlabel="Timestamp",ylabel="# of entries")
        self.TOTPlot =  MatplotlibHistoWidget(self.ui.TOTPlot,title="TOT distribution",xlabel="TOT",ylabel="# of entries")
        self.HitmapPlot =  MatplotlibMatrixWidget(self.ui.hitmap,title="Hit Map",xlabel="Columns",ylabel="rows",sizex=400,sizey=610)


        self.TDACplot.FillTDACMap("bleh")
        self.Maskplot.FillMaskMap("bleh")
        self.powerPlot.PlotPower(0)
        self.TS1Plot.PlotHisto(0)
        self.TOTPlot.PlotHisto(0)
        self.HitmapPlot.FillTDACMap("bleh")
        self.show()

        self.ui.MonitorPower.setCheckable(True)
        self.ui.MonitorPower.clicked.connect(self.MonitorPowerClicked)



def main():
    app = QApplication(sys.argv)
    ex = ATLASPixGUI()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()