#ifndef DEVICE_ATLASPIX2MATRIX_H
#define DEVICE_ATLASPIX2MATRIX_H

#include <array>
#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "ATLASPix2_Config.hpp"

enum class ATLASPix1Flavor { M1, M1Iso, M2, Undefined };

/** ATLASPix matrix configuration and related methods.
 *
 * Handles the shift register encoding and configuration files.
 */
struct ATLASPix2Matrix {
  std::unique_ptr<ATLASPix2_Config> VoltageDACConfig;
  std::unique_ptr<ATLASPix2_Config> CurrentDACConfig;
  std::unique_ptr<ATLASPix2_Config> MatrixDACConfig;

  // Voltage DACs
  double BLPix; // Voltage, to be translated to DAC value
  uint32_t nu2;
  double ThPix; // Voltage, to be translated to DAC value
  uint32_t nu3;
  double VMINUSPix, GatePix, GNDDACPix,VMinusPD,VNFBPix,VMain2,BLResPix;

  // TDAC and mask maps
  std::array<std::array<int, 36>, 24> TDAC; // last bit also encodes mask
  std::array<std::array<int, 36>, 24> MASK; // duplicate of last TDAC bit

  // info about matrix, SR etc...
  int ncol, nrow,ndoublecol;
  int nSRbuffer, nbits;
  int counter;
  uint32_t SRmask, extraBits, PulserMask;
  int maskx, masky;

  ATLASPix2Matrix();

  // initialize for M1 flavor
  void initialize();
  void _initializeGlobalParameters();
  void _initializeColumnParameters();
  void _initializeRowParameters();

  void setTDAC(uint32_t col, uint32_t row, uint32_t value);
  void setUniformTDAC(uint32_t value);
  void setMask(uint32_t col, uint32_t row, uint32_t value);

  /// Write global configuration file
  void writeGlobal(std::string basename) const;
  /// Write per-pixel trim dac configuration file
  void writeTDAC(std::string basename) const;
  /// Load global configuration file
  void loadGlobal(std::string basename);
  /// Load per-pixel trim dac configuration file
  void loadTDAC(std::string basename);

  /// encode shift register content as vector of 32bit words
  std::vector<uint32_t> encodeShiftRegister() const;
};

#endif // DEVICE_ATLASPIX2MATRIX_H
