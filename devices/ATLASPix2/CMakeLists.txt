PEARY_ENABLE_DEFAULT(ON)

# Define device and return the generated name as DEVICE_NAME
PEARY_DEVICE(DEVICE_NAME)

# Add source files to library
PEARY_DEVICE_SOURCES(${DEVICE_NAME}
    ATLASPix2Device.cpp
    ATLASPix2Matrix.cpp
    ATLASPix2_Config.cpp
)

# Provide standard install target
PEARY_DEVICE_INSTALL(${DEVICE_NAME})


INSTALL(TARGETS
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)
