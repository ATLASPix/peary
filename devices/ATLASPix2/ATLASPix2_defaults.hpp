#ifndef DEVICE_ATLASPix2_DEFAULTS_H
#define DEVICE_ATLASPix2_DEFAULTS_H

#include "ATLASPix2_clk_100-FMC_REF.h"
#include "carboard.hpp"
#include "dictionary.hpp"

namespace caribou {

// clang-format off
/** Default device path for this device: SEAF-connector I2C bus on BUS_I2C2
 */
#define DEFAULT_DEVICEPATH BUS_I2C2

/** Default I2C address for standalone-ATLASPix board with unconnected I2C address lines
 */
#define ATLASPix2_DEFAULT_I2C 0b0011000

  /** Definition of default values for the different DAC settings for ATLASPix
   */
#define ATLASPix2_VCC25 2.5
#define ATLASPix2_VCC25_CURRENT 2

#define ATLASPix2_VDDD 1.85
#define ATLASPix2_VDDD_CURRENT 2
#define ATLASPix2_VDDRam 1.85
#define ATLASPix2_VDDRam_CURRENT 2
#define ATLASPix2_VDDHigh 1.85
#define ATLASPix2_VDDHigh_CURRENT 2
#define ATLASPix2_VDDA 1.85
#define ATLASPix2_VDDA_CURRENT 2
#define ATLASPix2_VSSA 1.2
#define ATLASPix2_VSSA_CURRENT 2


#define ATLASPix2_GndDACPix 0
#define ATLASPix2_VMinusPix 0.65
#define ATLASPix2_GatePix 2.2

#define ATLASPix2_BLPix 0.8


#define ATLASPix2_ThPix 0.95


#define ATLASPix2_ncol 24
#define ATLASPix2_nrow 36

#define ATLASPix2_mask_X 1
#define ATLASPix2_mask_Y 400

#define TuningMaxCount 1000
#define Tuning_timeout 100

  // ATLASPix2  SR FSM control
  const std::intptr_t ATLASPix2_CONTROL_BASE_ADDRESS = 0x43C20000;
  const std::size_t ATLASPix2_CONTROL_MAP_SIZE = 4096;
  const std::uint32_t ATLASPix2_RAM_address_MASK = ATLASPix2_CONTROL_MAP_SIZE - 1;

  // ATLASPix2 Pulser Control
  const std::intptr_t ATLASPix2_PULSER_BASE_ADDRESS = 0x43C10000;
  const std::size_t ATLASPix2_PULSER_MAP_SIZE = 4096;
  const std::uint32_t ATLASPix2_PULSER_MASK = ATLASPix2_PULSER_MAP_SIZE - 1;

  // ATLASPix2 readout
  const std::intptr_t ATLASPix2_READOUT_BASE_ADDRESS = 0x43C00000;
  const std::size_t ATLASPix2_READOUT_MAP_SIZE = 16 * 4096;
  const std::uint32_t ATLASPix2_READOUT_MASK = ATLASPix2_READOUT_MAP_SIZE - 1;

  // ATLASPix Counter Control
  const std::intptr_t ATLASPix_COUNTER_BASE_ADDRESS = 0x43C70000;
  const std::size_t ATLASPix_COUNTER_MAP_SIZE = 4096;
  const std::uint32_t ATLASPix_COUNTER_MASK = ATLASPix_COUNTER_MAP_SIZE - 1;

  // ATLASPix readout
  const std::intptr_t ATLASPix_READOUT_BASE_ADDRESS = 0x43C70000;
  const std::size_t ATLASPix_READOUT_MAP_SIZE = 16 * 4096;
  const std::uint32_t ATLASPix_READOUT_MASK = ATLASPix_READOUT_MAP_SIZE - 1;

/** Dictionary for register address/name lookup for ATLASPix2
 */
#define ATLASPix2_REGISTERS				\
  {						\
    {"unlock", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"BLResPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ThResPix", register_t<>(0x26, 0xFF, false, true, true)},	\
    {"VNPix", register_t<>(0x26, 0xFF, false, true, true)},	\
    {"VNFBPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNFollPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNRegCasc", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VDel", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPComp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDAC", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNPix2", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"BLResDig", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNBiasPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPLoadPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNOutPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPVCO", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNVCO", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDelDclMux", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDelDclMux", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDelDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDelDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDelPreEmp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDelPreEmp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDcl", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNLVDS", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNLVDSDel", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPPump", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"nu", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"RO_res_n", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Ser_res_n", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Aur_res_n", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"sendcnt", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"resetckdivend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"maxcycend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"slowdownend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"timerend", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ckdivend2", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ckdivend", register_t<>(0x26, 0xFF, false, true, true)},              \
    {"VPRegCasc", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPRamp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNcompPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPFoll", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNDACPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VPBiasRec", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"VNBiasRec", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Invert", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelEx", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelSlow", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"EnPLL", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"TriggerDelay", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"Reset", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ConnRes", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelTest", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"SelTestOut", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"BLPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"nu2", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ThPix", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"nu3", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_mode", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"ro_enable", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"armduration", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_injection", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"edge_sel", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_enable", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"busy_when_armed", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"trigger_always_armed", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"t0_enable", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"gray_decode", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"send_fpga_ts", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"filter_hp", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"filter_weird_data", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"hw_masking", register_t<>(0x26, 0xFF, false, true, true)},		\
    {"temperature", register_t<>(0b00000101)},		\
  }


  const std::string AXI_registers[] = {"trigger_mode","ro_enable","armduration","trigger_injection"
		  ,"edge_sel","edge_sel","trigger_enable","busy_when_armed","trigger_always_armed","t0_enable",
		  "gray_decode","send_fpga_ts","filter_hp","filter_weird_data","hw_masking"};

  // clang-format on

  //{"ext_clk", register_t<>(0x26, 0xFF, false, true, true)},		\

} // namespace caribou

#endif /* DEVICE_ATLASPix2_DEFAULTS_H */
