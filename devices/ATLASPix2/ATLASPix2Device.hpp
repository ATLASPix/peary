/**
 * Caribou implementation for the ATLASPix2 chip
 */

#ifndef DEVICE_ATLASPix2_H
#define DEVICE_ATLASPix2_H

#include <algorithm>
#include <atomic>
#include <bitset>
#include <cstdlib>
#include <map>
#include <string>
#include <thread>

#include "ATLASPix2_defaults.hpp"
#include "ATLASPix2Matrix.hpp"
#include "device.hpp"
#include "i2c.hpp"
#include "pearydevice.hpp"


namespace caribou {

  struct pixelhit {

    uint32_t col = 0;
    uint32_t row = 0;
    uint32_t ts1 = 0;
    uint32_t ts2 = 0;
    uint64_t fpga_ts = 0;
    uint32_t tot = 0;
    uint32_t SyncedTS = 0;
    uint32_t triggercnt;
    uint32_t ATPbinaryCnt;
    uint32_t ATPGreyCnt;

    bool operator==(const pixelhit& hit){

    	if((col==hit.col) && (row == hit.row)){
    		return true;
    	}
    	else{
    		return false;
    	}
    }


  };

  typedef std::map<std::pair<int, int>, unsigned int> CounterMap;
  typedef std::map<std::pair<int, int>,double> TOTMap;

  /** ATLASPix Device class definition
   */
  class ATLASPix2Device : public pearyDevice<iface_i2c> {

  public:
    ATLASPix2Device(const caribou::Configuration config);
    ~ATLASPix2Device();

    std::string getName();

    void printBits(size_t const size, void const* const ptr);

    /** Initializer function for ATLASPix2
     */
    void configure();

    void lock();
    void unlock();


    void setThreshold(double threshold);
    void setVMinus(double vminus);

    /** Turn on the power supply for the ATLASPix2 chip
     */
    void powerUp();

    /** Turn off the ATLASPix2 power
     */
    void powerDown();

    /* power monitoring thread */
    void MonitorPower();
    void StopMonitorPower();


    /** Set output base directory for all files.
     */
    void setOutputDirectory(std::string);

    /** Start the data acquisition
     */
    void daqStart();

    /** Stop the data acquisition
     */
    void daqStop();

    /** Report power status
     */
    void powerStatusLog();

    void exploreInterface(){};

    void setOutput(std::string datatype);
    void SetScanningMask(uint32_t mx,uint32_t my);

    // Reset the chip
    // The reset signal is asserted for ~5us
    void reset();
    void resetFIFO();
    void isLocked();

    void configureClock();
    void getTriggerCount();
    uint32_t getTriggerCounter();


    double ReadTemperature();

    pearydata getDataBin();

    pearydata getData();
    pearydata getDataTO(int maskx, int masky);
    std::vector<pixelhit> getDataTOvector(uint32_t timeout=Tuning_timeout,bool noisescan=0);
    std::vector<pixelhit> getDataTimer(uint32_t timeout,bool to_nodata=false);
    void NoiseRun(double duration);

    void dataTuning(double vmax, int nstep, int npulses);
    void VerifyTuning(double vmin, double vmax, int npulses, int npoints);
    void TDACScan(int VNDAC,double vmin, double vmax, uint32_t npulses, uint32_t npoints);


    // void doSCurve(uint32_t col, uint32_t row, double vmin, double vmax, uint32_t npulses, uint32_t npoints);

    void doSCurvePixel(uint32_t col,uint32_t row,double vmin, double vmax, uint32_t npulses, uint32_t npoints);
    void doSCurves(double vmin, double vmax, uint32_t npulses, uint32_t npoints);
    void ComputeSCurves(ATLASPix2Matrix& matrix, double vmax, int nstep, int npulses, int tup, int tdown);
    void PulseTune(double target);
    void MeasureTOT(double vmin, double vmax, uint32_t npulses, uint32_t npoints);
    void AverageTOT(std::vector<pixelhit> data, uint32_t maskidx, uint32_t maskidy, TOTMap& tots);

    void ReapplyMask();
    void LoadTDAC(std::string filename);
    void setAllTDAC(uint32_t value);
    void MaskPixel(uint32_t col, uint32_t row);
    void FindHotPixels(uint32_t threshold);
    void WriteConfig(std::string name);
    void WriteFWRegistersAndBias(std::string name);
    void LoadConfig(std::string filename);

    // void doNoiseCurve(uint32_t col, uint32_t row);
    void pulse(uint32_t npulse, uint32_t tup, uint32_t tdown, double amplitude);
    void SetPixelInjection(uint32_t col, uint32_t row, bool ana_state, bool hb_state, bool inj_state);
    void SetPixelInjectionState(uint32_t col, uint32_t row, bool ana_state, bool hb_state, bool inj);

  private:
    void ProgramSR(const ATLASPix2Matrix& matrix);
    void setSpecialRegister(std::string name, uint32_t value);
    void writeOneTDAC(ATLASPix2Matrix& matrix, uint32_t col, uint32_t row, uint32_t value);
    void writeUniformTDAC(ATLASPix2Matrix& matrix, uint32_t value);
    void writeAllTDAC(ATLASPix2Matrix& matrix);
    void SetInjectionMask(uint32_t maskx, uint32_t masky, uint32_t state);
    void ResetWriteDAC();

    std::vector<pixelhit> CountHits(std::vector<pixelhit> data, uint32_t maskidx, uint32_t maskidy, CounterMap& counts);
    uint32_t CountHits(std::vector<pixelhit> data, uint32_t col, uint32_t row);


    void resetPulser();
    void setPulse(ATLASPix2Matrix& matrix, uint32_t npulse, uint32_t n_up, uint32_t n_down, double voltage);
    void sendPulse();

    // void tune(ATLASPix2Matrix& matrix, double vmax, int nstep, int npulses, bool tuning_verification);
    void LoadConfiguration(int matrix);

    void runDaq();
    void runMonitorPower();

    ATLASPix2Matrix theMatrix;
    int pulse_width;

    std::atomic_flag _daqContinue;
    std::atomic_flag _monitorPowerContinue;

    std::thread _daqThread;
    std::thread _monitorPowerThread;

    std::string _output_directory;
    std::string data_type;
    std::vector<pixelhit> hplist;

    // SW registers
    bool filter_hp=false;
    bool filter_weird_data=false;
    bool gray_decoding_state=false;
    bool HW_masking=false;
    bool ro_enable;
    bool busy_when_armed;
    uint32_t armduration;
    bool edge_sel,trigger_enable,trigger_always_armed,t0_enable,send_fpga_ts,trigger_injection,gray_decode;
  };

} // namespace caribou

#endif /* DEVICE_ATLASPix2_H */
